<?php
namespace SysX;

use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;
use SysX\Authentication\AuthenticationService;
// Define application environment
defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

return array(
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view'
        )
    ),
    'view_helpers' => array(
        'invokables' => array(
            'versioncontrol' => 'SysX\View\Helper\VersionControl'
        )
    ),
    'service_manager' => array(
        'factories' => array(
            'ErrorHandler' => function ($sm)
            {
                return new Service\ErrorHandler();
            },
            'AuthenticationService' => function ($sm)
            {
                $adapter = GlobalAdapterFeature::getStaticAdapter();
                $authenticationService = new AuthenticationService($adapter);
                $authenticationService->setServiceLocator($sm);
                return $authenticationService;
            },
            'SysX\Media\Db' => 'SysX\Media\Db\Factory',
            'SysX\Media\Storage' => 'SysX\Media\Storage\Factory',
            'SysX\Media\Entity\Store\StoreStrategy\ImageStoreStrategy' => 'SysX\Media\Entity\Store\StoreStrategy\ImageStoreStrategyFacotry'
        )
    )
);


