<?php
namespace SysXTests\Media\Entity\Type;

use Zend\Test\PHPUnit\Controller\AbstractControllerTestCase;
use SysX\Media\Entity\Type\Image;
use SysXTests\Support\ServiceManagerFactory;

/**
 * test case.
 */
class ImageTest extends AbstractControllerTestCase
{

    public function setUp()
    {
        $this->sl = ServiceManagerFactory::getServiceManager();
    }

    public function testImage()
    {
        $image = new Image();
        $image->setFileName('test');
        $image->setFileExtension('jpg');
        $image->setPath('test');

        $strategey = $this->getMock('SysX\Media\Entity\StoreStrategy\StoreStrategyInterface', array(
            'save',
            'delete'
        ));
        $strategey->expects($this->once())
            ->method('save');

        $image->setStorageStrategy($strategey);
        $image->save();
    }
}

