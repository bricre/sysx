<?php
namespace SysXTests\Media\Entity;

use SysX\Media\Entity\AbstractEntity as Base;

class Tester extends Base
{

    public function getType()
    {
        return 'Image';
    }
}

/**
 * test case.
 */
class AbstractAdapterTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Prepares the environment before running a test.
     */
    protected function setUp()
    {
        parent::setUp();

        // TODO Auto-generated AbstractAdapterTest::setUp()
    }

    /**
     * Cleans up the environment after running a test.
     */
    protected function tearDown()
    {
        // TODO Auto-generated AbstractAdapterTest::tearDown()
        parent::tearDown();
    }

    /**
     * Constructs the test case.
     */
    public function __construct()
    {
        // TODO Auto-generated constructor
    }

    public function testMediaStorageAbstractAdapter()
    {
        $tester = new Tester('test', 'html', 'test');
        $this->assertEquals($tester->getUri()
            ->toString(), 'test/test.html');
    }
}

