<?php
namespace SysXTests\Media\Storage\Adapter;

use SysX\Media\Storage\Adapter\AbstractAdapter as Base;
use SysXTests\Media\Entity as TestEntity;

class Tester extends Base
{
    /*
     * (non-PHPdoc) @see \SysX\Media\Storage\Adapter\AdapterInterface::deleteEntity()
     */
    public function deleteEntity(\SysX\Media\Entity\EntityInterface $entity)
    {
        // TODO Auto-generated method stub
    }

    /*
     * (non-PHPdoc) @see \SysX\Media\Storage\Adapter\AdapterInterface::getEntityFile()
     */
    public function getEntityFile(\SysX\Media\Entity\EntityInterface $entity)
    {
        // TODO Auto-generated method stub
    }

    /*
     * (non-PHPdoc) @see \SysX\Media\Storage\Adapter\AdapterInterface::saveEntity()
     */
    public function saveEntity(\SysX\Media\Entity\EntityInterface $entity)
    {
        // TODO Auto-generated method stub
    }
}

/**
 * test case.
 */
class AbstractAdapterTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Prepares the environment before running a test.
     */
    protected function setUp()
    {
        parent::setUp();

        // TODO Auto-generated AbstractAdapterTest::setUp()
    }

    /**
     * Cleans up the environment after running a test.
     */
    protected function tearDown()
    {
        // TODO Auto-generated AbstractAdapterTest::tearDown()
        parent::tearDown();
    }

    /**
     * Constructs the test case.
     */
    public function __construct()
    {
        // TODO Auto-generated constructor
    }

    public function testMediaStorageAbstractAdapter()
    {
        $testEntity = new TestEntity\Tester('test', 'html', 'test');
        $config = array(
            'host' => 'localhost',
            'port' => null,
            'scheme' => 'http',
            'path' => ''
        );
        $tester = new Tester($config);
        $this->assertEquals($tester->getEntityUri($testEntity)
            ->toString(), 'http://localhost/test/test.html');
        $config['scheme'] = null;

        $tester = new Tester($config);
        $this->assertEquals($tester->getEntityUri($testEntity)
            ->toString(), '//localhost/test/test.html');
    }
}

