<?php
namespace SysXTests\Support;

use Zend\ServiceManager\ServiceManager;
use Zend\Mvc\Service\ServiceManagerConfig;

/**
 * Utility used to retrieve a freshly bootstrapped application's service manager
 *
 * @see https://github.com/doctrine/DoctrineORMModule/blob/0.7.0/tests/DoctrineORMModuleTest/Util/ServiceManagerFactory.php
 */
class ServiceManagerFactory
{

    /**
     *
     * @var array
     */
    protected static $config;

    /**
     *
     * @param array $config
     */
    public static function setConfig(array $config)
    {
        static::$config = $config;
    }

    /**
     * Builds a new service manager
     */
    public static function getServiceManager()
    {
        $serviceManager = new ServiceManager(new ServiceManagerConfig(isset(static::$config['service_manager']) ? static::$config['service_manager'] : array()));
        $serviceManager->setService('ApplicationConfig', static::$config);
        $serviceManager->setFactory('ServiceListener', 'Zend\Mvc\Service\ServiceListenerFactory');

        $serviceManager->setAllowOverride(true);
        return $serviceManager;
    }
}

?>