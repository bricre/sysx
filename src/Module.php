<?php
namespace SysX;

use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module implements ServiceProviderInterface
{

    public function onBootstrap(MvcEvent $e)
    {
        $EventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($EventManager);
        $EventManager->attach(
            MvcEvent::EVENT_DISPATCH_ERROR,
            function (MvcEvent $e) {
                $e->getApplication()
                    ->getServiceManager()
                    ->get('ErrorHandler')
                    ->handle($e);
            }
        );

        // $EventManager->attach(\Zend\Mvc\MvcEvent::EVENT_DISPATCH_ERROR, array(
        // $this,
        // 'handleError'
        // ), 1);

        $db = $e->getApplication()
            ->getServiceManager()
            ->get('Zend\Db\Adapter\Adapter');
        GlobalAdapterFeature::setStaticAdapter($db);
    }

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Zend\Log' => 'MpaFirephpWrapper\Service\FirephpFactory'
            )
        );
    }

    public function handleError(MvcEvent $e)
    {
        $exception = $e->getParam('exception');
        $e->getApplication()
            ->getServiceManager()
            ->get('Zend\Log')
            ->write($exception);
    }
}
