<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace SysX\Paginator\Adapter;

use SysX\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;

class DbTableGateway extends DbSelect
{

    public function __construct(TableGateway $tableGateway, Select $select)
    {
        $sql = $tableGateway->getSql();

        parent::__construct($select, $sql, $tableGateway->getResultSetPrototype());
    }

    public function count()
    {
        if ($this->rowCount !== null) {
            return $this->rowCount;
        }

        $select = clone $this->select;
        $select->reset(Select::LIMIT);
        $select->reset(Select::OFFSET);
        $select->reset(Select::ORDER);
        $select->resetJoin(
            array(
                Select::JOIN_LEFT,
                Select::JOIN_RIGHT
            )
        );

        $countSelect = new Select();
        $countSelect->columns(
            array(
                'c' => new Expression('COUNT(1)')
            )
        );
        $countSelect->from(
            array(
                'original_select' => $select
            )
        );

        $statement = $this->sql->prepareStatementForSqlObject($countSelect);
        $result    = $statement->execute();
        $row       = $result->current();

        $this->rowCount = $row['c'];

        return $this->rowCount;
    }
}
