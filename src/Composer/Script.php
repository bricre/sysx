<?php
namespace SysX\Composer;

use Composer\Config;
use Composer\Script\Event;
use Composer\Util\Filesystem;

class Script
{

    static $rootPath;

    static $vendorPath;

    public static function preAutoloadDump(/** @noinspection PhpUnusedParameterInspection */
        Event $event)
    {
        $vendorPath    = static::getVendorPath();
        $filesToRemove = array(
            //minify's FirePHP version is outdated and conflicts with official FirePHP which we use.
            $vendorPath . '/mrclay/minify/min/lib/FirePHP.php'
        );

        $filesystem = new Filesystem();

        foreach ($filesToRemove as $file) {
            echo 'Removing ' . $file . "\n";
            $filesystem->remove($file);
        }
    }

    public static function getRootPath()
    {
        if (!static::$rootPath) {
            static::$rootPath = realpath(
                __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR
            );
        }
        return static::$rootPath;
    }

    public static function getVendorPath()
    {
        if (!static::$vendorPath) {
            $config             = new Config();
            $vendorDir          = $config->get('vendor-dir');
            static::$vendorPath = realpath(
                static::getRootPath() . DIRECTORY_SEPARATOR . $vendorDir . DIRECTORY_SEPARATOR
            );
        }
        return static::$vendorPath;
    }
}