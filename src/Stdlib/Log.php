<?php
namespace SysX\Stdlib;

use Zend\Log as ZendLog;

/**
 * General trait to provide standard logging functionaility.
 * All classes uses this trait must implement Zend\ServiceManager\ServiceLocatorAwareInterface
 */
trait Log
{

    /**
     *
     * @return ZendLog
     */
    protected function getLogger()
    {
        return $this->getServiceLocator()->get('Zend\Log');
    }

    protected function log($message)
    {
        $this->getLogger()->write($message);
    }
}