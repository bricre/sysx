<?php
namespace SysX\Form;

use Zend\Form\Annotation\AnnotationBuilder;
use Zend\Form\Element\Select;
use Zend\Form\Form as ZendForm;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

abstract class AbstractForm extends ZendForm implements InputFilterAwareInterface, ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const MODEL_CLASS = 'NOT_DEFINED_CLASS';

    public function __construct()
    {
        if ('NOT_DEFINED_CLASS' == static::MODEL_CLASS) {
            throw new \Exception('Model Class not defined for Form creation.');
        }

        parent::__construct();
    }

    public function prepare()
    {
        if ($this->isPrepared) {
            return $this;
        }

        $AnnotationBuilder = new AnnotationBuilder();
        $fieldset          = $AnnotationBuilder->createForm(static::MODEL_CLASS);

        $elements = $fieldset->getElements();
        foreach ($elements as $Element) {
            $Element->setOptions(
                array(
                    'column-size'      => 'sm-8',
                    'label_attributes' => array(
                        'class' => 'col-sm-3'
                    )
                )
            );
            if ($Element instanceof Select) {
                if ($Element->getAttribute('value_tablegateway')) {
                    $valueTableGatewayClass = $Element->getAttribute('value_tablegateway');
                    $valueTableGateway      = new $valueTableGatewayClass();
                    $valueTableGateway->setServiceLocator($this->getServiceLocator());
                    $Element->setOption('value_options', $valueTableGateway->getSelectOptions());
                }
                $Element->setEmptyOption(
                    $this->getServiceLocator()
                        ->get('translator')
                        ->translate('Please select an option')
                );
                $Element->setDisableInArrayValidator(true);
            }
            $this->add($Element);
        }

        $this->setInputFilter($fieldset->getInputFilter());
        $this->setHydrator($fieldset->getHydrator());
        $this->setName($fieldset->getName());

        $this->add(
            array(
                'name' => 'csrf',
                'type' => 'csrf'
            )
        )->add(
                array(
                    'name'       => 'submit',
                    'type'       => 'button',
                    'attributes' => array(
                        'type'  => 'Submit',
                        'class' => 'btn-primary'
                    ),
                    'options'    => array(
                        'label'       => 'Save',
                        'glyphicon'   => 'ok-circle',
                        'column-size' => 'sm-2 col-sm-offset-2'
                    )
                )
            );

        return parent::prepare();
    }
}

?>