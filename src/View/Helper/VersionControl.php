<?php
namespace SysX\View\Helper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\View\Helper\AbstractHelper;

class VersionControl extends AbstractHelper implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    public static $version;

    public function __invoke()
    {
        if (!self::$version) {
            self::$version = $this->getVersion();
        }
        return self::$version;
    }

    public function getVersion()
    {
        // /public/index.php should have changed current working directory to root of this application
        $root = getcwd();

        // If we use git as version control system
        if (file_exists($root . DIRECTORY_SEPARATOR . '.git')) {
            return filemtime($root . DIRECTORY_SEPARATOR . '.git');
        } // fallback to Mercurial
        elseif ($root . DIRECTORY_SEPARATOR . '.hg') {
            return filemtime($root . DIRECTORY_SEPARATOR . '.hg');
        } // No version control available, fallback to last root folder modifiled time.
        else {
            return filemtime($root);
        }
    }
}