<?php
namespace SysX\Media\Entity;

use SysX\Media\Entity\StoreStrategy\DoNotStoreStrategy;
use SysX\Media\Entity\StoreStrategy\StoreStrategyInterface;
use SysX\Media\Storage\Exception\StorageUnavailableException;
use Zend\Uri\UriFactory;

abstract class AbstractEntity implements EntityInterface
{

    /**
     *
     * @var string
     */
    protected $path;

    /**
     *
     * @var string
     */
    protected $fileName;

    /**
     *
     * @var string
     */
    protected $fileExtension;

    /**
     *
     * @var string
     */
    protected $type;

    /**
     * File content if current entity is a file
     *
     * @var mixed
     */
    protected $content;

    /**
     *
     * @var array
     */
    protected $rawData = array();

    /**
     *
     * @var \SysX\Media\Storage\Strategy\StrategyInterface
     */
    protected $storageStrategy = null;

    /**
     *
     * @param string|array $fileName
     *            Filename or config array to setup this entity
     * @param string $fileExtension
     * @param string $path
     */
    public function __construct($fileName = null, $fileExtension = null, $path = null)
    {
        if (is_array($fileName)) {
            if (isset($fileName['fileName'])) {
                $fileName = $fileName['fileName'];
            }
            if (isset($fileName['fileExtension'])) {
                $fileExtension = $fileName['fileExtension'];
            }
            if (isset($FielName['path'])) {
                $path = $fileName['path'];
            }
        }
        $this->setFileName($fileName);
        $this->setFileExtension($fileExtension);
        $this->setPath($path);
        $this->setStorageStrategy(new DoNotStoreStrategy());
    }

    /**
     * (non-PHPdoc)
     *
     * @see \SysX\Media\Entity\EntityInterface::getUri()
     * @return \Zend\Uri\Uri
     */
    public function getUri()
    {
        return UriFactory::factory(
            $this->getPath() . '/' . $this->getFileName() .
            ($this->getFileExtension() ? '.' . $this->getFileExtension() : '')
        );
    }

    /**
     *
     * @return the $path
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     *
     * @return the $fileName
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     *
     * @return the $rawData
     */
    public function getRawData()
    {
        return $this->rawData;
    }

    /**
     *
     * @param multitype : $rawData
     */
    public function setRawData($rawData)
    {
        $this->rawData = $rawData;
        return $this;
    }

    /**
     *
     * @return the $fileExtension
     */
    public function getFileExtension()
    {
        return $this->fileExtension;
    }

    /**
     *
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     *
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     *
     * @param string $fileExtension
     */
    public function setFileExtension($fileExtension)
    {
        $this->fileExtension = $fileExtension;
    }

    /**
     *
     * @return the $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     *
     * @return \SysX\Media\Entity\StoreStrategy\StrategyInterface $storageStrategy
     */
    public function getStorageStrategy()
    {
        return $this->storageStrategy;
    }

    /**
     *
     * @param \SysX\Media\Entity\StoreStrategy\StrategyInterface $storageStrategy
     */
    public function setStorageStrategy(StoreStrategyInterface $storageStrategy)
    {
        $this->storageStrategy = $storageStrategy;
        return $this;
    }

    /**
     *
     * @return the $content
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     *
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    public function save()
    {
        if (!$this->getStorageStrategy()) {
            throw new StorageUnavailableException('Storage not defined!');
        }
        $this->getStorageStrategy()->save($this);
    }
}