<?php
namespace SysX\Media\Entity\StoreStrategy;

use SysX\Media\Entity\EntityInterface;

class ImageStoreStrategy extends BasicStoreStrategy
{

    public function delete(EntityInterface $entity)
    {
        if ($this->dbAdapter) {
            $this->dbAdapter->delete($entity);
        }
        if ($this->storageAdapter) {
            $this->storageAdapter->deleteEntity($entity);
        }
        return true;
    }

    public function save(EntityInterface $entity)
    {
        if ($this->dbAdapter) {
            $this->dbAdapter->save($entity);
        }
        if ($this->storageAdapter) {
            $this->storageAdapter->saveEntity($entity);
        }
        return true;
    }
}

?>