<?php
namespace SysX\Media\Entity\StoreStrategy;

use SysX\Media\Entity\EntityInterface;

interface StoreStrategyInterface
{

    public function save(EntityInterface $entity);

    public function delete(EntityInterface $entity);
}

?>