<?php
namespace SysX\Media\Entity\StoreStrategy;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ImageStoreStrategyFactory implements FactoryInterface
{

    public function createService(
        ServiceLocatorInterface $serviceLocator
    )
    {
        $strategy = new ImageStoreStrategy();
        /** @noinspection PhpParamsInspection */
        $strategy->setDbAdapter($serviceLocator->get('SysX\Media\Db'));
        /** @noinspection PhpParamsInspection */
        $strategy->setStorageAdapter($serviceLocator->get('SysX\Media\Storage'));
        return $strategy;
    }
}

?>