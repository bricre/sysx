<?php
namespace SysX\Media\Entity\StoreStrategy;

use SysX\Media\Entity\EntityInterface;

class DoNotStoreStrategy implements StoreStrategyInterface
{

    public function delete(EntityInterface $entity)
    {
        return true;
    }

    public function save(EntityInterface $entity)
    {
        return true;
    }
}

?>