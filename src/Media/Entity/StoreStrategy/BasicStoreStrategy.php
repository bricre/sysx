<?php
namespace SysX\Media\Entity\StoreStrategy;

use SysX\Media\Db\DbAwareInterface;
use SysX\Media\Db\DbAwareTrait;
use SysX\Media\Entity\EntityInterface;
use SysX\Media\Storage\StorageAdapterAwareInterface;
use SysX\Media\Storage\StorageAdapterAwareTrait;

class BasicStoreStrategy implements StoreStrategyInterface, DbAwareInterface, StorageAdapterAwareInterface
{
    use DbAwareTrait, StorageAdapterAwareTrait;

    public function delete(EntityInterface $entity)
    {
        if ($this->dbAdapter) {
            $this->dbAdapter->delete($entity);
        }
        if ($this->storageAdapter) {
            $this->storageAdapter->deleteEntity($entity);
        }
        return true;
    }

    public function save(EntityInterface $entity)
    {
        if ($this->dbAdapter) {
            $this->dbAdapter->save($entity);
        }
        if ($this->storageAdapter) {
            $this->storageAdapter->saveEntity($entity);
        }
        return true;
    }
}

?>