<?php
namespace SysX\Media\Entity;

interface EntityInterface
{

    public function getUri();

    public function getType();

    public function setType($type);

    public function getFileName();

    public function setFileName($fileName);

    public function getRawData();

    public function setRawData($rawData);
}