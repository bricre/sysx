<?php
namespace SysX\Media\Entity\Type;

use SysX\Media\Entity\AbstractEntity;

class Image extends AbstractEntity
{

    const TYPE = __CLASS__;

    public function getType()
    {
        return static::TYPE;
    }
}

?>