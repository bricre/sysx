<?php
namespace SysX\Media\Storage;

use SysX\Media\Storage\Adapter\AdapterInterface;

interface StorageAdapterAwareInterface
{

    function setStorageAdapter(AdapterInterface $dapter);
}