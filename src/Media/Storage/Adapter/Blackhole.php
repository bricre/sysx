<?php
namespace SysX\Media\Storage\Adapter;

/**
 * Do not actually save any files
 *
 * @author allan
 *
 */
class Blackhole extends AbstractAdapter
{

    public function deleteEntity(\SysX\Media\Entity\EntityInterface $entity)
    {
        return true;
    }

    public function getEntityFile(\SysX\Media\Entity\EntityInterface $entity)
    {
        return null;
    }

    public function saveEntity(\SysX\Media\Entity\EntityInterface $entity)
    {
        return true;
    }
}

?>