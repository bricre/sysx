<?php
namespace SysX\Media\Storage\Adapter;

use SysX\Media\Entity\EntityInterface;
use Zend\Uri\Uri;

abstract class AbstractAdapter implements AdapterInterface
{

    /**
     * URI Scheme
     *
     * @var string
     */
    protected $scheme;

    /**
     * URI Host
     *
     * @var string
     */
    protected $host;

    /**
     * URI Path
     *
     * @var string
     */
    protected $path;

    /**
     * URI Port
     *
     * @var string
     */
    protected $port;

    public function __construct($config = array())
    {
        $this->setHost($config['host'])
            ->setPath($config['path'])
            ->setPort($config['port'])
            ->setScheme($config['scheme']);
    }

    /**
     * Build a accessible URI
     *
     * @param EntityInterface $entity
     * @return \Zend\Uri\Uri
     */
    public function getEntityUri(EntityInterface $entity)
    {
        $uri = new Uri();
        if ($this->getScheme()) {
            $uri->setScheme($this->getScheme());
        }
        if ($this->getHost()) {
            $uri->setHost($this->getHost());
        }
        if ($this->getPort()) {
            $uri->setPort($this->getPort());
        }

        $uri->setPath(
            $this->getPath() . '/' . $entity->getUri()
                ->toString()
        );

        return $uri;
    }

    /**
     *
     * @return the $scheme
     */
    public function getScheme()
    {
        return $this->scheme;
    }

    /**
     *
     * @return the $host
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     *
     * @return the $path
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     *
     * @return the $port
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     *
     * @param string $scheme
     */
    public function setScheme($scheme)
    {
        $this->scheme = $scheme;
        return $this;
    }

    /**
     *
     * @param string $host
     */
    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    /**
     *
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     *
     * @param string $port
     */
    public function setPort($port)
    {
        $this->port = $port;
        return $this;
    }
}