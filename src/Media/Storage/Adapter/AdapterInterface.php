<?php
namespace SysX\Media\Storage\Adapter;

use SysX\Media\Entity\EntityInterface;

interface AdapterInterface
{

    /**
     * Get file resource to manipulate with
     *
     * @param EntityInterface $entity
     */
    public function getEntityFile(EntityInterface $entity);

    /**
     * Save the entity to storage
     *
     * @param EntityInterface $entity
     */
    public function saveEntity(EntityInterface $entity);

    /**
     * Delete the entity from storage
     *
     * @param EntityInterface $entity
     */
    public function deleteEntity(EntityInterface $entity);
}