<?php
namespace SysX\Media\Storage;

use SysX\Media\Storage\Adapter\AdapterInterface;

trait StorageAdapterAwareTrait
{

    /**
     *
     * @var \SysX\Media\Storage\Adapter\AdapterInterface
     */
    protected $storageAdapter;

    public function setStorageAdapter(AdapterInterface $adapter)
    {
        $this->storageAdapter = $adapter;
    }
}