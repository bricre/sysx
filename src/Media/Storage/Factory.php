<?php
namespace SysX\Media\Storage;

use SysX\Media\Storage\Adapter\Local;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Factory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');
        $config = $config['SysX\Media'];
        if (!array_key_exists('Storage', $config)) {
            throw new \InvalidArgumentException("'Storage' not defined for Media\\Storage\\Adapter\\Local");
        }

        return new Local($config['Storage']);
    }
}

?>