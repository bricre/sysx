<?php
namespace SysX\Media\Db;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Factory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $db     = $serviceLocator->get('db');
        $config = $serviceLocator->get('Config');
        $config = $config['SysX\Media'];
        if (!array_key_exists('Db', $config) && array_key_exists('table', $config['Db'])) {
            throw new \InvalidArgumentException("'table' not defined for Media\\Db\\TableGateway");
        }

        return new TableGateway($config['Db']['table'], $db);
    }
}

?>