<?php
namespace SysX\Media\Db;

use Media\Entity\TypeEnum;
use SysX\Media\Db\Exception\InvalidEntityTypeException;
use SysX\Media\Entity\Db\Hydrator;
use Zend\Db\ResultSet\HydratingResultSet;

/**
 * All results must contain COLUMN_FILE_NAME, it should either be the name of file
 * or URL of a link
 *
 * COLUMN_FILE_EXTENSION is optional but should be set when TYPE is kind of file
 * COLUMN_PATH is optional but should be set when TYPE is kind of file
 *
 * COLUMN_TYPE is optional, when it's null we assume the whole table
 * contains only one spcific type
 *
 * @author allan
 *
 */
class ResultSet extends HydratingResultSet
{

    const COLUMN_PATH = 'path';

    const COLUMN_FILE_NAME = 'file_name';

    const COLUMN_FILE_EXTENSION = 'file_extension';

    const COLUMN_TYPE = 'type';

    protected $options = array(
        self::COLUMN_FILE_NAME      => 'file_name',
        self::COLUMN_FILE_EXTENSION => null,
        self::COLUMN_PATH           => null,
        self::COLUMN_TYPE           => null
    );

    private $typeEnum;

    public function __construct()
    {
        $this->typeEnum = TypeEnum::$classes;
        $this->setHydrator(new Hydrator());
    }

    public function current()
    {
        if ($this->buffer === null) {
            $this->buffer = -2; // implicitly disable buffering from here on
        } elseif (is_array($this->buffer) && isset($this->buffer[$this->position])) {
            return $this->buffer[$this->position];
        }
        $data = $this->dataSource->current();

        if (!$data[$this->options[static::COLUMN_FILE_NAME]]) {
            throw new InvalidEntityTypeException('Record doesn\'t have a valie file name field.');
        }
        if (!$data[$this->options[static::COLUMN_FILE_EXTENSION]]) {
            throw new InvalidEntityTypeException('Record doesn\'t have a valie file extension field.');
        }
        if (!$data[$this->options[static::COLUMN_PATH]]) {
            throw new InvalidEntityTypeException('Record doesn\'t have a valie path field.');
        }

        if (null !== static::COLUMN_TYPE && $type = $data[$this->options[static::COLUMN_TYPE]]) {
            $this->setObjectPrototype(new $this->typeEnum[strtolower($type)]());
        } elseif (!$this->objectPrototype) {
            throw new InvalidEntityTypeException('Record doesn\'t have a valid type field, nor default type given.');
        }

        $object = is_array($data) ? $this->hydrator->hydrate($data, clone $this->objectPrototype, $this) : false;

        if (is_array($this->buffer)) {
            $this->buffer[$this->position] = $object;
        }

        return $object;
    }

    public function setOptions(array $options)
    {
        foreach ($options as $key => $value) {
            if (key_exists($key, $this->options)) {
                $this->options[$key] = $value;
            }
        }
    }

    public function getOption($option)
    {
        if (!array_key_exists($option, $this->options)) {
            throw new \InvalidArgumentException('Option is invalid.');
        }
        return $this->options[$option];
    }

    public function getOptions()
    {
        return $this->options;
    }
}

?>