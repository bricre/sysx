<?php
namespace SysX\Media\Db;

trait DbAwareTrait
{

    /**
     *
     * @var \SysX\Media\Db\TableGateway
     */
    protected $dbAdapter;

    public function setDbAdapter(TableGateway $dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
        return $this;
    }

    public function getDbAdapter()
    {
        return $this->dbAdapter;
    }
}

?>