<?php
namespace SysX\Media\Db;

interface DbAwareInterface
{

    public function setDbAdapter(TableGateway $dbApter);

    public function getDbAdapter();
}

?>