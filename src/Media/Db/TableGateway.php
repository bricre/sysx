<?php
namespace SysX\Media\Db;

use SysX\Media\Storage\StorageAdapterAwareInterface;
use SysX\Media\Storage\StorageAdapterAwareTrait;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSetInterface;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway as Base;

class TableGateway extends Base implements StorageAdapterAwareInterface
{
    use StorageAdapterAwareTrait;

    public function __construct(
        $table,
        AdapterInterface $adapter,
        $features = null,
        ResultSetInterface $resultSetPrototype = null,
        Sql $sql = null
    )
    {
        parent::__construct($table, $adapter, $features, $resultSetPrototype, $sql);
    }
}