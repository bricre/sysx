<?php
namespace SysX\Media\Entity\Db;

use SysX\Media\Db\ResultSet;
use SysX\Media\Entity\EntityInterface;
use Zend\Stdlib\Hydrator\AbstractHydrator;

class Hydrator extends AbstractHydrator
{

    public function extract(EntityInterface $object, ResultSet $resultSet)
    {
        $data    = array();
        $rawData = $object->getRawData();
        foreach ($rawData as $key => $value) {
            $data[$key] = $value;
        }

        $data[$resultSet->getOption(ResultSet::COLUMN_FILE_NAME)] = $object->getFileName();

        if (null !== $resultSet->getOption(ResultSet::COLUMN_TYPE)) {
            $data[$resultSet->getOption(ResultSet::COLUMN_TYPE)] = $object->getType();
        }
        if (null !== $resultSet->getOption(ResultSet::COLUMN_FILE_EXTENSION)) {
            $data[$resultSet->getOption(ResultSet::COLUMN_FILE_EXTENSION)] = $object->getFileExtension();
        }
        if (null !== $resultSet->getOption(ResultSet::COLUMN_PATH)) {
            $data[$resultSet->getOption(ResultSet::COLUMN_PATH)] = $object->getPath();
        }

        return $data;
    }

    public function hydrate(array $data, EntityInterface $object, ResultSet $resultSet)
    {
        $object->setFileName($data[$resultSet->getOption(ResultSet::COLUMN_FILE_NAME)]);

        if (null !== $resultSet->getOption(ResultSet::COLUMN_TYPE)) {
            $object->setType($data[$resultSet->getOption(ResultSet::COLUMN_TYPE)]);
        }
        if (null !== $resultSet->getOption(ResultSet::COLUMN_FILE_EXTENSION)) {
            $object->setFileExtension($data[$resultSet->getOption(ResultSet::COLUMN_FILE_EXTENSION)]);
        }
        if (null !== $resultSet->getOption(ResultSet::COLUMN_PATH)) {
            $object->setPath($data[$resultSet->getOption(ResultSet::COLUMN_PATH)]);
        }

        $object->setRawData($data);
        return $object;
    }
}

?>