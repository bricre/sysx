<?php
namespace SysX\Service\ErrorHandler;

use Zend\EventManager\EventInterface;

interface ErrorHandlerInterface
{

    public function handle(EventInterface $event);
}