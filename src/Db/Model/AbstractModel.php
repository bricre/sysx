<?php
namespace SysX\Db\Model;

use Zend\Stdlib\ArraySerializableInterface;

abstract class AbstractModel implements ArraySerializableInterface
{

    public function exchangeArray(array $array)
    {
        foreach ($array as $key => $value) {
            if ($value) {
                $method = 'set' . $this->beautify($key);

                if (is_callable(
                    array(
                        $this,
                        $method
                    )
                )
                ) {
                    $this->$method($value);
                }
            }
        }
        return $this;
    }

    public function getArrayCopy()
    {
        $Relection  = new \ReflectionClass($this);
        $properties = $Relection->getProperties(\ReflectionProperty::IS_PROTECTED);
        $result     = array();
        foreach ($properties as $Property) {
            if ('csrf' == $Property->getName()) {
                continue;
            }

            $method = 'get' . $this->beautify($Property->getName());
            if (is_callable(
                array(
                    $this,
                    $method
                )
            )
            ) {
                $result[$Property->getName()] = $this->$method();
            }
        }
        return $result;
    }

    private function beautify($underscored_text)
    {
        return ucfirst(
            preg_replace_callback(
                '@\_([a-z1-9]*)@',
                function ($matches) {
                    return ucfirst($matches[1]);
                },
                $underscored_text
            )
        );
    }

    public function getModelRecordTitle()
    {
        if (@$this->title) {
            return $this->title;
        }
        if (@$this->name) {
            return $this->name;
        }
        return null;
    }
}