<?php
namespace SysX\Db\TableGateway;

use SysX\Db\Sql\Select;
use SysX\Stdlib\Log;
use Zend\Db\Metadata\Metadata;
use Zend\Db\Metadata\Object\TableObject;
use Zend\Db\TableGateway\Feature;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

abstract class AbstractTableGateway extends TableGateway implements ServiceLocatorAwareInterface
{

    use Log, ServiceLocatorAwareTrait;

    /**
     *
     * @var string Model name used in this TableGateway
     */
    protected $model;

    /**
     *
     * @var array
     */
    private $primaryKeys = null;

    /**
     *
     * @var TableObject
     */
    private $metadata = null;

    /**
     *
     * @var \Zend\Db\Sql\Where[]
     */
    protected $formSelectOptionsWhere = array();

    public function __construct()
    {
        $adapter = Feature\GlobalAdapterFeature::getStaticAdapter();
        return parent::__construct($this->table, $adapter, null, null);
    }

    public function insert($set)
    {
        $columns = $this->getMetadata()->getColumns();
        $data    = array();
        foreach ($columns as $column) {
            if (isset($set[$column->getName()])) {
                $data[$column->getName()] = $set[$column->getName()];
            }
        }

        return parent::insert($data);
    }

    public function update($set, $where = null)
    {
        $columns = $this->getMetadata()->getColumns();
        $data    = array();
        foreach ($columns as $column) {
            if (isset($set[$column->getName()])) {
                $data[$column->getName()] = $set[$column->getName()];
            }
        }

        return parent::update($data, $where);
    }

    public function select($where = null)
    {
        if (!$this->isInitialized) {
            $this->initialize();
        }

        $select = $this->getSelect($where);
        return $this->selectWith($select);
    }

    /**
     *
     * @param array|string|\Closure $where
     * @return \SysX\Db\Sql\Select
     */
    public function getSelect($where = null)
    {
        $select = new Select($this->table);
        $this->preSelect($select, $where);
        if ($where instanceof \Closure) {
            $where($select);
        } elseif ($where !== null) {
            $select->where($where);
        }

        $this->postSelect($select, $where);
        return $select;
    }

    protected function preSelect(Select $select, $where = null)
    {
    }

    protected function postSelect(Select $select, $where = null)
    {
    }

    /**
     *
     * @return \Zend\Db\Metadata\Metadata\Object\TableObject
     */
    public function getMetadata()
    {
        $cache    = $this->getServiceLocator()->get('cache-dbtable-metadata');
        $cacheKey = $this->getTable() . '_METADATA';
        if ($cache->hasItem($cacheKey)) {
            $this->metadata = $cache->getItem($cacheKey);
        }

        if (null === $this->metadata) {

            $this->adapter->getDriver()
                ->getConnection()
                ->execute('set global innodb_stats_on_metadata=0;');

            $metadata       = new Metadata($this->adapter);
            $this->metadata = $metadata->getTable($this->getTable());

            $this->adapter->getDriver()
                ->getConnection()
                ->execute('set global innodb_stats_on_metadata=1;');

            $cache->addItem($cacheKey, $this->metadata);
        }
        return $this->metadata;
    }

    /**
     * Get PrimaryKeys from current table.
     * We need to manually set innodb_stats_on_metadata=0 when doing this query otherwise it's exremely slow
     *
     * @see http://www.mysqlperformanceblog.com/2011/12/23/solving-information_schema-slowness/
     * @return multitype:
     */
    public function getPrimaryKeys()
    {
        $cache    = $this->getServiceLocator()->get('cache-dbtable-metadata');
        $cacheKey = $this->getTable() . '_METADATA_PRIMARY_KEYS';
        if ($cache->hasItem($cacheKey)) {
            $this->primaryKeys = $cache->getItem($cacheKey);
        }
        if (null === $this->primaryKeys) {

            $this->adapter->getDriver()
                ->getConnection()
                ->execute('set global innodb_stats_on_metadata=0;');

            $metadata    = new Metadata($this->adapter);
            $constraints = $metadata->getTable($this->getTable())
                ->getConstraints();

            $this->adapter->getDriver()
                ->getConnection()
                ->execute('set global innodb_stats_on_metadata=1;');

            foreach ($constraints as $constraint) {
                if ($constraint->isPrimaryKey()) {
                    $primaryColumns    = $constraint->getColumns();
                    $this->primaryKeys = $primaryColumns;
                }
            }

            $cache->addItem($cacheKey, $this->primaryKeys);
        }

        return $this->primaryKeys;
    }

    /**
     * Return a key=>value array for Zend\Form\Element\Select to use as value_options
     *
     * @see \Zend\Form\Element\Select
     * @see \SysX\Form\AbstractForm::prepare()
     * @return array
     */
    public function getFormSelectOptions()
    {
        $models      = $this->select($this->getFormSelectOptionsWhere());
        $options     = array();
        $primaryKeys = $this->getPrimaryKeys();
        foreach ($models as $model) {
            $primaryKeyValues = array();
            $modelValues      = $model->getArrayCopy();
            foreach ($primaryKeys as $primaryKey) {
                $primaryKeyValues[] = $modelValues[$primaryKey];
            }
            $primaryKey           = implode('-', $primaryKeyValues);
            $options[$primaryKey] = $model->getModelRecordTitle();
        }
        return $options;
    }

    /**
     *
     * @return \Zend\Db\Sql\Where[]
     */
    public function getFormSelectOptionsWhere()
    {
        return $this->formSelectOptionsWhere;
    }

    /**
     * Set criterias before using getSelectOptions
     *
     * @param \Zend\Db\Sql\Where[] $where
     * @return \SysX\Db\TableGateway\AbstractTableGateway
     */
    public function setFormSelectOptionsWhere($where)
    {
        $this->formSelectOptionsWhere = $where;
        return $this;
    }
}