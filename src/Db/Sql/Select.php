<?php
namespace SysX\Db\Sql;

use Zend\Db\Sql\Select as Base;
use Zend\Uri\Exception\InvalidUriPartException;

class Select extends Base
{

    public function resetJoin($types)
    {
        if (!is_array($types)) {
            $types = array(
                $types
            );
        }

        foreach ($this->joins as $key => $value) {
            foreach ($types as $type) {
                switch ($type) {
                    case self::JOIN_LEFT:
                    case self::JOIN_RIGHT:
                    case self::JOIN_INNER:
                    case self::JOIN_OUTER:
                        unset($this->joins[$key]);
                        break;
                    default:
                        throw new InvalidUriPartException('Unsupported Join Type on resetting Select::joins');
                        break;
                }
            }
        }
        return $this;
    }
}