<?php
namespace SysX\Authentication;

use SysX\Db\Model\AbstractModel;
use Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter;
use Zend\Authentication\Storage\Session;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class AuthenticationService extends CredentialTreatmentAdapter implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * $credentialTreatment - Treatment applied to the credential, such as MD5() or PASSWORD()
     *
     * @var string
     */
    protected $credentialTreatment = 'MD5(?)';

    /**
     * $tableName - the table name to check
     *
     * @var string
     */
    protected $tableName = 'user';

    /**
     * $identityColumn - the column to use as the identity
     *
     * @var string
     */
    protected $identityColumn = 'username';

    /**
     * $credentialColumns - columns to be used as the credentials
     *
     * @var string
     */
    protected $credentialColumn = 'password';

    /**
     *
     * @var Session
     */
    private $session;

    /**
     * $resultPrototype - Prototype for $identityModel
     *
     * @var AbstractModel
     */
    private $resultPrototype;

    /**
     * $identityModel - A Model object represent DB Table row
     *
     * @var AbstractModel Object
     */
    private $identityModel;

    public function __construct(DbAdapter $zendDb, $tableName = null, $identityColumn = null, $credentialColumn = null)
    {
        parent::__construct($zendDb, $tableName, $identityColumn, $credentialColumn);
        $this->session = new Session();
    }

    /**
     *
     * @return boolean
     */
    public function isAuthenticated()
    {
        return !$this->session->isEmpty();
    }

    public function authenticate()
    {
        $result = parent::authenticate();
        if (true === $result->isValid()) {
            $this->prepareIdentityModel();
            $this->session->write($this->getIdentityModel());
        }
        return $result->isValid();
    }

    public function deAuthenticate()
    {
        $this->session->clear();
    }

    public function setResultSetPrototype(AbstractModel $resultSetPrototype)
    {
        $this->resultSetPrototype = $resultSetPrototype;
    }

    public function getIdentityModel()
    {
        if (false === $this->session->isEmpty()) {
            return $this->session->read();
        } else {
            return $this->identityModel;
        }
    }

    private function prepareIdentityModel()
    {
        if ($this->resultPrototype) {
            $this->identityModel = $this->resultPrototype->exchangeArray($this->resultRow);
        } else {
            $this->identityModel = $this->getResultRowObject();
        }
    }
}