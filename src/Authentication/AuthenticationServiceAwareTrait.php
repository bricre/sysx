<?php
namespace SysX\Authentication;

trait AuthenticationServiceAwareTrait
{

    /**
     *
     * @var AuthenticationService
     */
    protected $authenticationService;

    /**
     * Set the Authentication Service
     *
     * @param AuthenticationService $authenticationService
     */
    public function setAuthenticationService(AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    /**
     *
     * @return AuthenticationService $authenticationService
     */
    public function getAuthenticationService()
    {
        return $this->authenticationService;
    }
}