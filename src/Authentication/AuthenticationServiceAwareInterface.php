<?php
namespace SysX\Authentication;

interface AuthenticationServiceAwareInterface
{

    /**
     * Set the Authentication Service
     *
     * @param AuthenticationService $authenticationService
     */
    public function setAuthenticationService(AuthenticationService $authenticationService);

    /**
     * @return AuthenticationService $authenticationService
     */
    public function getAuthenticationService();
}