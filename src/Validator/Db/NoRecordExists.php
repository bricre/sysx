<?php
namespace SysX\Validator\Db;

use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;
use Zend\Validator\Db\NoRecordExists as Base;

class NoRecordExists extends Base
{

    public function __construct($options = null)
    {
        parent::__construct($options);
        $this->adapter = $this->getAdapter();
    }

    public function getAdapter()
    {
        if (!$this->adapter) {
            $this->adapter = GlobalAdapterFeature::getStaticAdapter();
        }
        return $this->adapter;
    }
}