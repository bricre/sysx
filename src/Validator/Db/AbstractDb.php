<?php
namespace SysX\Validator\Db;

use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;
use Zend\Validator\Db\AbstractDb as Base;

abstract class AbstractDb extends Base
{

    public function getAdapter()
    {
        if (!$this->adapter) {
            $this->adapter = GlobalAdapterFeature::getStaticAdapter();
        }
        return $this->adapter;
    }
}