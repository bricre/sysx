<?php
namespace SysX\Mvc;

use Zend\Mvc\MvcEvent as Base;

class MvcEvent extends Base
{

    /**
     * #@+
     * Mvc events triggered by eventmanager
     */
    const EVENT_ACTION_INDEX_PRE = 'action.index.pre';

    const EVENT_ACTION_INDEX_POST = 'action.index.post';

    const EVENT_ACTION_EDIT_PRE = 'action.edit.pre';

    const EVENT_ACTION_EDIT_POST = 'action.edit.post';

    const EVENT_ACTION_DELETE_PRE = 'action.delete.pre';

    const EVENT_ACTION_DELETE_POST = 'action.delete.post';

    /**
     * #@-
     */
}