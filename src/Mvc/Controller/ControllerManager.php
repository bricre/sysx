<?php
namespace SysX\Mvc\Controller;

use Zend\Mvc\Controller\ControllerManager as Base;

class ControllerManager extends Base
{

    protected $autoAddInvokableClass = true;

    public function get($name, $options = array(), $usePeeringServiceManagers = true)
    {
        // Allow specifying a class name directly; registers as an invokable class
        if (!$this->has($name) && $this->autoAddInvokableClass && class_exists($name)) {
            $this->setInvokableClass($name, $name);
        }

        return parent::get($name, $options, $usePeeringServiceManagers);
    }
}