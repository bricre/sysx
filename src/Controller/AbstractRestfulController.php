<?php
namespace SysX\Controller;

use SysX\Controller\AbstractControllerTrait;
use SysX\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractRestfulController as Base;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\Parameters;

/**
 *
 * @property \Zend\View\Model\JsonModel $viewModel
 * @property \SysX\Db\TableGateway\AbstractTableGateway $tableGateway
 * @property \SysX\Form\AbstractForm $form
 */
abstract class AbstractRestfulController extends Base implements AbstractControllerInterface
{
    use AbstractControllerTrait {
        AbstractControllerTrait::onDispatch as parentOnDispatch;
        AbstractControllerTrait::indexAction as parentIndexAction;
        AbstractControllerTrait::editAction as parentEditAction;
        AbstractControllerTrait::createAction as parentCreateAction;
        AbstractControllerTrait::deleteAction as parentDeleteAction;
    }

    public function onDispatch(MvcEvent $e)
    {
        $acceptCriteria = array(
            'Zend\View\Model\JsonModel' => array(
                'text/html',
                'application/json'
            )
        );

        $this->viewModel = $this->acceptableViewModelSelector($acceptCriteria);

        Json::$useBuiltinEncoderDecoder = true;

        $events = $e->getTarget()->getEventManager();
        /** @noinspection PhpUndefinedMethodInspection */
        $events->attach(
            'editAction.pre',
            function ($e) {
                /** @noinspection PhpUndefinedMethodInspection */
                if ($e->getTarget()->form) {
                    /** @noinspection PhpUndefinedMethodInspection */
                    $e->getTarget()->form->remove('csrf');
                    /** @noinspection PhpUndefinedMethodInspection */
                    $e->getTarget()->form->getInputFilter()
                        ->remove('csrf');
                }
            }
        );
        /** @noinspection PhpUndefinedMethodInspection */
        $events->attach(
            'createAction.pre',
            function ($e) {
                /** @noinspection PhpUndefinedMethodInspection */
                if ($e->getTarget()->form) {
                    /** @noinspection PhpUndefinedMethodInspection */
                    $e->getTarget()->form->remove('csrf');
                    /** @noinspection PhpUndefinedMethodInspection */
                    $e->getTarget()->form->getInputFilter()
                        ->remove('csrf');
                }
            }
        );
        return self::parentOnDispatch($e);
    }

    public function get($id)
    {
        return self::indexAction();
    }

    public function getList()
    {
        return self::indexAction();
    }

    public function create($data)
    {
        // Convert POST content as POST so ZF can handle it in generic way
        $params = array();
        parse_str($this->getRequest()->getContent(), $params);
        $params = array_merge($params, $this->params()->fromRoute());
        /** @noinspection PhpUndefinedMethodInspection */
        $this->getRequest()->setPost(new Parameters($params));

        $primaryKeys = $this->getPrimaryKeys();
        if ($primaryKeys) {
            return self::parentEditAction();
        }

        self::parentCreateAction();
        /** @noinspection PhpUndefinedFieldInspection */
        unset($this->viewModel->form);

        $primaryKeys      = $this->tableGateway->getPrimaryKeys();
        $primaryKeyValues = (array)$this->viewModel->getVariable('data', null);
        $where            = array();
        foreach ($primaryKeys as $index => $primaryKey) {
            $where[$primaryKey . ' = ?'] = $primaryKeyValues[$index];
        }

        /** @noinspection PhpUndefinedMethodInspection */
        $this->viewModel->setVariable(
            'data',
            $this->tableGateway->select($where)
                ->current()
                ->getArrayCopy()
        );
        return $this->viewModel;
    }

    public function update($id, $data)
    {
        // Convert PUT content as POST so ZF can handle it in generic way
        $params = array();
        parse_str($this->getRequest()->getContent(), $params);
        $params = array_merge($params, $this->params()->fromRoute());
        /** @noinspection PhpUndefinedMethodInspection */
        $this->getRequest()->setPost(new Parameters($params));

        self::parentEditAction();
        /** @noinspection PhpUndefinedFieldInspection */
        unset($this->viewModel->form);
        return $this->viewModel;
    }

    public function replaceList($data)
    {
        return $this->update(null, $data);
    }

    public function delete($id)
    {
        return self::parentDeleteAction();
    }

    public function indexAction()
    {
        self::parentIndexAction();

        /** @noinspection PhpUndefinedFieldInspection */
        if ($paginator = $this->viewModel->paginator) {
            /** @noinspection PhpUndefinedFieldInspection */
            $this->viewModel->total = $paginator->getTotalItemCount();
            /** @noinspection PhpUndefinedFieldInspection */
            $this->viewModel->data = $paginator->getCurrentItems();
            /** @noinspection PhpUndefinedFieldInspection */
            unset($this->viewModel->paginator);
        }
        return $this->viewModel;
    }

    public function editAction()
    {
        self::parentEditAction();
        /** @noinspection PhpUndefinedFieldInspection */
        unset($this->viewModel->form);
        return $this->viewModel;
    }

    public function prepareSelect(Select $select)
    {
        $select = $this->prepareSelectSetExtJsFilters($select);
        return $select;
    }

    private function prepareSelectSetExtJsFilters(Select $select)
    {
        $params = $this->params()->fromQuery();

        if (!empty($params['filter'])) {
            $filters = Json::decode($params['filter'], Json::TYPE_ARRAY);
            foreach ($filters as $filter) {
                // Stupid ExtJS use 'property' as property name in Store Filter
                // And 'field' as property name in Grid Filter, I have to standarlise them
                $filter['field'] = empty($filter['property']) ? $filter['field'] : $filter['property'];

                if (empty($filter['field']) || !isset($filter['value'])) {
                    continue;
                }

                $filter['field'] = str_replace('-', '.', $filter['field']);

                if (!@strlen($filter['field'])) {
                    continue;
                }

                if (is_string($filter['value'])) {
                    $filter['value'] = trim($filter['value']);
                    if (0 == strlen($filter['value'])) {
                        continue;
                    }
                }

                if (!strstr($filter['field'], '.')) {
                    $filter['field'] = $this->tableGateway->getTable() . '.' . $filter['field'];
                }
                switch (@$filter['type']) {
                    case 'string':
                        $select->where(
                            array(
                                $filter['field'] . " LIKE ?" => '%' . $filter['value'] . '%'
                            )
                        );
                        break;
                    case 'boolean':
                        $select->where(
                            array(
                                $filter['field'] . " = ?" => $filter['value'] == true ? 1 : 0
                            )
                        );
                        break;
                    case 'numeric':
                        $operatorMap = array(
                            'ne' => '!=',
                            'eq' => '=',
                            'lt' => '<=',
                            'gt' => '>='
                        );
                        $select->where(
                            array(
                                $filter['field'] . ' ' . $operatorMap[$filter['comparison']] . ' ?' => $filter['value']
                            )
                        );
                        break;
                    case 'date':
                        $operatorMap = array(
                            'ne' => '!=',
                            'eq' => '=',
                            'lt' => '<',
                            'gt' => '>'
                        );
                        if ($filter['comparison'] == 'eq') {
                            $select->where(
                                array(
                                    $filter['field'] .
                                    " BETWEEN '? 00:00:00' AND '? 23:59:59'" => new Expression($filter['value'])
                                )
                            );
                        } else {
                            $select->where(
                                array(
                                    $filter['field'] . ' ' . $operatorMap[$filter['comparison']] .
                                    ' ?' => new Expression("'" . $filter['value'] . "'")
                                )
                            );
                        }
                        break;
                    case 'list':
                        $select->where(
                            array(
                                $filter['field'] => $filter['value']
                            )
                        );
                        break;
                    default:
                        $select->where(
                            array(
                                $filter['field'] . ' = ?' => $filter['value']
                            )
                        );
                }
            }
        }
        return $select;
    }
}

?>