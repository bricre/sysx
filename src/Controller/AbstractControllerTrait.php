<?php
namespace SysX\Controller;

use SysX\Paginator\Adapter\DbTableGateway as TableGatewayPaginatorAdapter;
use SysX\Stdlib\Log;
use Zend\Db\Adapter\ParameterContainer;
use Zend\Mvc\MvcEvent;
use Zend\Paginator\Paginator;
use Zend\Stdlib\ArraySerializableInterface;
use Zend\View\ViewModel;

trait AbstractControllerTrait
{

    use Log;

    /**
     *
     * @var \SysX\Db\TableGateway\AbstractTableGateway $tableGateway
     */
    protected $tableGateway;

    /**
     *
     * @var \SysX\Form\AbstractForm $form
     */
    protected $form;

    /**
     *
     * @var \Zend\View\Model\ViewModel $viewModel
     */
    protected $viewModel;

    /**
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        $this->getEventManager()->trigger(__FUNCTION__ . '.pre', $this);
        if ($this->tableGateway) {
            $primaryKeys = null;
            try {
                $primaryKeys = $this->getPrimaryKeys(true);
            } catch (\InvalidArgumentException $e) {
            }

            if ($primaryKeys) {
                $data = $this->tableGateway->select($primaryKeys)->current();

                if (!$data) {
                    throw new \InvalidArgumentException($this->_('Cannot find requested record'));
                }
                if ($data instanceof ArraySerializableInterface) {
                    $this->viewModel->setVariable('data', $data->getArrayCopy());
                } else {
                    $this->viewModel->setVariable('data', $data);
                }
                $this->viewModel->setVariable('success', true);
            } else {
                $select                       = $this->prepareSelect($this->tableGateway->getSelect());
                $tableGatewayPaginatorAdapter = new TableGatewayPaginatorAdapter($this->tableGateway, $select);
                $paginator                    = new Paginator($tableGatewayPaginatorAdapter);
                $paginator->setItemCountPerPage(
                    (int)$this->params()
                        ->fromQuery('limit', 50)
                );
                $paginator->setCurrentPageNumber(
                    (int)$this->params()
                        ->fromQuery('page', 1)
                );
                $paginator->setItemCountPerPage(10);
                $this->viewModel->setVariable('paginator', $paginator);
                $this->viewModel->setVariable('success', true);
            }
        }
        $this->getEventManager()->trigger(__FUNCTION__ . '.post', $this);
        return $this->viewModel;
    }

    /**
     * Perform basic logics for Create Action
     *
     * @return \Zend\View\ViewModel
     */
    public function createAction()
    {
        $this->initForm();

        $this->getEventManager()->trigger(__FUNCTION__ . '.pre', $this);
        if ($this->form) {
            $primaryKeys = $this->tableGateway->getPrimaryKeys();
            foreach ($primaryKeys as $primaryKey) {
                $this->form->remove($primaryKey);
                $this->form->getInputFilter()->remove($primaryKey);
            }
        }

        if ($this->tableGateway) {
            $primaryKeys = $this->getPrimaryKeys();
            if ($primaryKeys) {
                throw new \InvalidArgumentException($this->_('You cannot specify primary keys for create action'));
            }

            if (true === $this->getRequest()->isPost()) {
                $data = $this->getRequest()
                    ->getPost()
                    ->toArray();
                $this->form->setData($data);
                if ($this->form->isValid()) {
                    $this->tableGateway->insert($data);
                    $this->viewModel->setVariable('success', true);
                    $this->viewModel->setVariable('data', $this->tableGateway->getLastInsertValue());
                } else {
                    $this->viewModel->setVariable('success', false);
                    $this->viewModel->setVariable(
                        'messages',
                        $this->form->getInputFilter()
                            ->getMessages()
                    );
                    $this->log(
                        $this->form->getInputFilter()
                            ->getMessages()
                    );
                }
            }
            $this->viewModel->setVariable('form', $this->form);
        }
        $this->getEventManager()->trigger(__FUNCTION__ . '.post', $this);

        return $this->viewModel;
    }

    /**
     * Perform basic logics for Edit Action
     *
     * @return \Zend\View\ViewModel
     */
    public function editAction()
    {
        $this->initForm();
        $this->getEventManager()->trigger(__FUNCTION__ . '.pre', $this);

        if ($this->tableGateway) {
            $primaryKeys = $this->getPrimaryKeys(true);

            $Model = $this->tableGateway->select($primaryKeys)->current();

            if (!$Model) {
                throw new \InvalidArgumentException($this->_('Cannot find requested record'));
            }

            $this->form->bind($Model);
            if (true === $this->getRequest()->isPost() || true === $this->getRequest()->isPut()) {
                $this->form->setData(
                    $this->getRequest()
                        ->getPost()
                        ->toArray()
                );
                if ($this->form->isValid()) {
                    $this->tableGateway->update($Model->getArrayCopy(), $primaryKeys);
                    $this->viewModel->setVariable('success', true);
                } else {
                    $this->viewModel->setVariable('success', false);
                    $this->viewModel->setVariable(
                        'messages',
                        $this->form->getInputFilter()
                            ->getMessages()
                    );
                    $this->log(
                        $this->form->getInputFilter()
                            ->getMessages()
                    );
                }
            }
            $this->viewModel->setVariable('form', $this->form);
        }
        $this->getEventManager()->trigger(__FUNCTION__ . '.post', $this);

        return $this->viewModel;
    }

    /**
     * Perform basic logics for Delete Action
     *
     * @return \Zend\View\ViewModel
     */
    public function deleteAction()
    {
        $this->getEventManager()->trigger(__FUNCTION__ . '.pre', $this);

        if ($this->tableGateway) {
            $primaryKeys = $this->getPrimaryKeys(true);

            if ($this->tableGateway->delete($primaryKeys)) {
                $this->viewModel->setVariable('success', true);
            } else {
                $this->viewModel->setVariable('success', false);
            }
            $this->viewModel->setVariable('form', $this->form);
        }
        $this->getEventManager()->trigger(__FUNCTION__ . '.post', $this);

        return $this->viewModel;
    }

    public function onDispatch(MvcEvent $e)
    {
        $this->getEventManager()->trigger(__FUNCTION__ . '.pre', $this);
        $this->initTableGateway();

        $actionResponse = parent::onDispatch($e);
        $this->getEventManager()->trigger(__FUNCTION__ . '.post', $this);
        $this->logDbQueries();
        return $actionResponse;
    }

    protected function initTableGateway()
    {
        if (null !== $this->tableGateway) {
            if (is_string($this->tableGateway)) {
                $this->tableGateway = new $this->tableGateway();
                $this->tableGateway->setServiceLocator($this->getServiceLocator());
            }
        }
        return $this;
    }

    protected function initForm()
    {
        if (null !== $this->form) {
            if (is_string($this->form)) {
                $this->form = new $this->form();
                $this->form->setServiceLocator($this->getServiceLocator());
                $this->form->prepare();
            }
        }
        return $this;
    }

    private function logDbQueries()
    {
        $profiler = $this->getServiceLocator()
            ->get('Zend\Db\Adapter\Adapter')
            ->getProfiler();
        if ($profiler) {
            $queryProfiles = $profiler->getProfiles();
            if (count($queryProfiles) > 0) {
                $logs   = array();
                $logs[] = array(
                    'Time',
                    'SQL',
                    'Parameters'
                );
                foreach ($queryProfiles as $QueryProfile) {
                    $log = array(
                        $QueryProfile['elapse'],
                        $QueryProfile['sql']
                    );

                    if ($QueryProfile['parameters'] instanceof ParameterContainer) {
                        $log[] = $QueryProfile['parameters']->getNamedArray();
                    } else {
                        $log = $QueryProfile['parameters'];
                    }

                    $logs[] = $log;
                }

                $this->getLogger()->write(
                    'SQL Queries',
                    'table',
                    $logs,
                    array(
                        'Collapsed' => false
                    )
                );
            }
        }
    }

    /**
     * Shortcut to access translator service
     *
     * @param string $message
     */
    public function _($message)
    {
        return $this->getServiceLocator()
            ->get('translator')
            ->translate($message);
    }

    /**
     * Validate and return primarykeys from request
     * If no primarykey existed will return false
     *
     * @return array bool
     */
    protected function getPrimaryKeys($requireExist = false)
    {
        $params      =
            array_merge($this->params()->fromRoute(), $this->params()->fromQuery(), $this->params()->fromPost());
        $primaryKeys = $this->tableGateway->getPrimaryKeys();
        $result      = array();
        foreach ($primaryKeys as $primaryKey) {
            if (key_exists($primaryKey, $params) && isset($params[$primaryKey]) && strlen($params[$primaryKey]) > 0) {
                $result[$this->tableGateway->getTable() . '.' . $primaryKey] = $params[$primaryKey];
            } else {
                if ($requireExist) {
                    throw new \InvalidArgumentException(sprintf(
                                                            $this->_("Required parameter '%s' not given!"),
                                                            $primaryKey
                                                        ));
                }
            }
        }
        return count($result) > 0 ? $result : false;
    }
}