<?php
namespace SysX\Controller;

use SysX\Controller\AbstractControllerTrait;
use Zend\Mvc\Controller\AbstractActionController as Base;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;

/**
 *
 * @property \Zend\View\Model\ViewModel $viewModel
 * @property \SysX\Db\TableGateway\AbstractTableGateway $tableGateway
 * @property \SysX\Form\AbstractForm $form
 */
abstract class AbstractActionController extends Base
{
    use AbstractControllerTrait {
        AbstractControllerTrait::onDispatch as parentOnDispatch;
        AbstractControllerTrait::indexAction as parentIndexAction;
        AbstractControllerTrait::editAction as parentEditAction;
        AbstractControllerTrait::createAction as parentCreateAction;
        AbstractControllerTrait::deleteAction as parentDeleteAction;
    }

    public function indexAction()
    {
        $ViewModel = new ViewModel();
        $ViewModel->setTemplate('general/index');
        $ViewModel->setVariable('paginator', self::parentIndexAction());
        return $ViewModel;
    }

    public function editAction()
    {
        self::parentEditAction();
        if ($this->form) {
            if (true === $this->getRequest()->isPost()) {
                if (true == $this->viewModel->getVariable('success', false)) {
                    return $this->redirect()->toRoute(
                        $this->getEvent()
                            ->getRouteMatch()
                            ->getMatchedRouteName(),
                        array(
                            'action' => 'index'
                        )
                    );
                }
            }
        }
        $this->viewModel->setTemplate('general/edit');
        return $this->viewModel;
    }

    public function createAction()
    {
        self::parentCreateAction();
        if ($this->form) {
            if (true === $this->getRequest()->isPost()) {
                if (true == $this->viewModel->getVariable('success', false)) {
                    return $this->redirect()->toRoute(
                        $this->getEvent()
                            ->getRouteMatch()
                            ->getMatchedRouteName(),
                        array(
                            'action' => 'index'
                        )
                    );
                }
            }
        }
        $this->viewModel->setTemplate('general/create');
        return $this->viewModel;
    }

    public function deleteAction()
    {
        self::parentDeleteAction();
        if (true == $this->viewModel->getVariable('success', false)) {
            return $this->redirect()->toRoute(
                $this->getEvent()
                    ->getRouteMatch()
                    ->getMatchedRouteName(),
                array(
                    'action' => 'index'
                )
            );
        }
        $this->viewModel->setTemplate('general/delete');
        return $this->viewModel;
    }

    protected function setModuleTemplatePath($path = null)
    {
        if (!$path) {
            $path = __DIR__ . '/../../view/';
        }
        $path = realpath($path);

        $templatePathResolver = $this->getServiceLocator()->get('Zend\View\Resolver\TemplatePathStack');
        $templatePathResolver->setLfiProtection(false);
        $templatePathResolver->addPath($path);
    }

    public function onDispatch(MvcEvent $e)
    {
        $this->viewModel = $this->acceptableviewmodelselector(
            array(
                'Zend\View\Model\ViewModel' => array(
                    'text/html'
                )
            )
        );
        return self::parentOnDispatch($e);
    }
}
