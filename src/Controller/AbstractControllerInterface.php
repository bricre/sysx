<?php
namespace SysX\Controller;

use SysX\Db\Sql\Select;

interface AbstractControllerInterface
{

    function prepareSelect(Select $select);
}